<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function checkAuth(Request $request)
    {
        $auth = $this->validate($request, [
            'email' => 'required | email',
            'password' => 'required'
        ]);

        if (Auth::attempt($auth))
            return response()->json(['msg' => 'Authorization Successful'], 200);

        return response()->json(['msg' => 'Authorization Failed'], 401);
    }
}
