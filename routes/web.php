<?php


Route::group(['middleware' => 'guest'], function () {

    Route::get('/', function () {
        return view('welcome');
    })->name('mainlogin');

    Route::post('/checklogin', 'AuthController@checkAuth')->name('checkAuth');
});

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', function () {
        echo 'Home Page';
    })->name('home');

});
