<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.5.2/css/bulma.css">

</head>
<body>
<div class="container" id="root">
    <div class="notification is-primary" v-if="errors.invalid != ''">
        <button class="delete" @click="hideNotification"></button>
        @{{ errors.invalid }}
    </div>
    <form action="{{ route('checkAuth') }}" method="POST" @submit.prevent="checkAuth">
        <div class="field">
            <label class="label">Username</label>
            <div class="control has-icons-left has-icons-right">
                <input class="input is-success" type="text" placeholder="Email"
                       v-model="email" @keydown="clearEmailErrors">
                <span class="icon is-small is-left">
                    <i class="fa fa-user"></i>
                </span>
            </div>
            <p class="help is-danger" v-if="errors.email != ''" v-text="errors.email"></p>
        </div>
        <div class="field">
            <label class="label">Password</label>
            <div class="control has-icons-left has-icons-right">
                <input class="input is-success" type="password" placeholder="Password" v-model="password"
                       @keydown="clearPasswordErrors">
                <span class="icon is-small is-left">
                    <i class="fa fa-key"></i>
                </span>
            </div>
            <p class="help is-danger" v-if="errors.password != ''" v-text="errors.password"></p>
        </div>

        <div class="field is-grouped">
            <div class="control">
                <button class="button is-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
