new Vue({
    el: '#root',
    data: {
        email: '',
        password: '',
        errors: {
            invalid: '',
            email:'',
            password:''
        }
    },

    methods: {
        checkAuth(){
            axios.post('/checklogin', {
                email: this.email,
                password: this.password
            })
                .then(response => {
                    if(response.data.msg == 'Authorization Successful')
                    window.location.href="home";
            })
            .catch(error => {
                if(error.response.status == 422){
                    if(error.response.data.errors.email){
                        this.errors.email = error.response.data.errors.email[0]
                    }
                    if(error.response.data.errors.password){
                        this.errors.password = error.response.data.errors.password[0]
                    }
                }
                if(error.response.status == 401){
                    this.errors.invalid = error.response.data.msg;
                }

        });
        },

        clearEmailErrors(){
            this.errors.email = '';
        },

        clearPasswordErrors(){
            this.errors.password = '';
        },

        hideNotification(){
            this.errors.invalid = '';
        }
    }
});